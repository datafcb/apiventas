dotnet lambda deploy-function --function-name ApiVentas_GET_PaymentMethod --function-handler LambdaGet::LambdaGet.Functions::GetPaymentMethodsAsync
dotnet lambda deploy-function --function-name ApiVentas_GET_Pack --function-handler LambdaGet::LambdaGet.Functions::GetPacksAsync
dotnet lambda deploy-function --function-name ApiVentas_GET_Promotion --function-handler LambdaGet::LambdaGet.Functions::GetPromotionsAsync
dotnet lambda deploy-function --function-name ApiVentas_GET_Category --function-handler LambdaGet::LambdaGet.Functions::GetCategoriesAsync
dotnet lambda deploy-function --function-name ApiVentas_GET_SalesStatus --function-handler LambdaGet::LambdaGet.Functions::GetSalesStatusAsync
dotnet lambda deploy-function --function-name ApiVentas_GET_SalePoint --function-handler LambdaGet::LambdaGet.Functions::GetSalePointsAsync