﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using LambdaModel;
using LambdaModel.DynamoDB;
using Newtonsoft.Json;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]
namespace LambdaGet
{
    public class Functions
    {
        IDynamoDBContext DDBContext { get; set; }

        private Dictionary<string, string> responseHeaders = new Dictionary<string, string>
            {{"Content-Type", "application/json"}, {"Access-Control-Allow-Origin", "*"}};
        
        public Functions()
        {
            AWSConfigsDynamoDB.Context.TypeMappings[typeof(PaymentMethod)] = new Amazon.Util.TypeMapping(typeof(PaymentMethod),  "ApiVentas_paymentMethod");
            AWSConfigsDynamoDB.Context.TypeMappings[typeof(Pack)] = new Amazon.Util.TypeMapping(typeof(Pack),  "ApiVentas_pack");
            AWSConfigsDynamoDB.Context.TypeMappings[typeof(Promotion)] = new Amazon.Util.TypeMapping(typeof(Promotion),  "ApiVentas_promotion");
            AWSConfigsDynamoDB.Context.TypeMappings[typeof(Category)] = new Amazon.Util.TypeMapping(typeof(Category),  "ApiVentas_category");
            AWSConfigsDynamoDB.Context.TypeMappings[typeof(SalesStatus)] = new Amazon.Util.TypeMapping(typeof(SalesStatus),  "ApiVentas_salesStatus");
            AWSConfigsDynamoDB.Context.TypeMappings[typeof(SalePoint)] = new Amazon.Util.TypeMapping(typeof(SalePoint),  "ApiVentas_salePoint");
            var config = new DynamoDBContextConfig { Conversion = DynamoDBEntryConversion.V2 };
            this.DDBContext = new DynamoDBContext(new AmazonDynamoDBClient(), config);
        }
        
        /// <summary>
        /// Get all payment method from database
        /// </summary>
        /// <returns>List<Pack></returns>
        public async Task<APIGatewayProxyResponse> GetPaymentMethodsAsync(ILambdaContext context)
        {
            try
            {
                context.Logger.LogLine("GetPaymentMethodAsync");
                var search = DDBContext.ScanAsync<PaymentMethod>(null);
                List<PaymentMethod> page = await search.GetNextSetAsync();
                context.Logger.LogLine("Returned: "+page.Count);
                
                var response = new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.OK,
                    Body = JsonConvert.SerializeObject(page),
                    Headers = responseHeaders
                };
                return response;
            }
            catch (Exception ex)
            {
                context.Logger.LogLine(ex.ToString());
                var response = new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    Body = JsonConvert.SerializeObject(new Error("Internal server error")),
                    Headers = responseHeaders
                };
                return response;
            }

        }
        
        /// <summary>
        /// Get all packs from database
        /// </summary>
        /// <returns>List<Pack></returns>
        public async Task<APIGatewayProxyResponse> GetPacksAsync(ILambdaContext context)
        {
            try
            {
                context.Logger.LogLine("GetPacksAsync");
                var search = this.DDBContext.ScanAsync<Pack>(null);
                List<Pack> page = await search.GetNextSetAsync();
                context.Logger.LogLine("Returned: "+page.Count);
                var response = new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.OK,
                    Body = JsonConvert.SerializeObject(page),
                    Headers = responseHeaders
                };
                return response;
            }
            catch (Exception ex)
            {
                context.Logger.LogLine(ex.ToString());
                var response = new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    Body = JsonConvert.SerializeObject(new Error("Internal server error")),
                    Headers = responseHeaders
                };
                return response;
            }
        }
        
        /// <summary>
        /// Get all promotions from database
        /// </summary>
        /// <returns>List<Promotion></returns>
        public async Task<APIGatewayProxyResponse> GetPromotionsAsync(ILambdaContext context)
        {
            try
            {
                context.Logger.LogLine("GetPromotionsAsync");
                var search = this.DDBContext.ScanAsync<Promotion>(null);
                List<Promotion> page = await search.GetNextSetAsync();
                context.Logger.LogLine("Returned: "+page.Count);
                var response = new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.OK,
                    Body = JsonConvert.SerializeObject(page),
                    Headers = responseHeaders
                };
                
                return response;
            }
            catch (Exception ex)
            {
                context.Logger.LogLine(ex.ToString());
                var response = new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    Body = JsonConvert.SerializeObject(new Error("Internal server error")),
                    Headers = responseHeaders
                };
                return response;
            }
        }
        
        /// <summary>
        /// Get all categories from database
        /// </summary>
        /// <returns>List<Category></returns>
        public async Task<APIGatewayProxyResponse> GetCategoriesAsync(ILambdaContext context)
        {
            try
            {
                context.Logger.LogLine("GetCategoriesAsync");
                var search = this.DDBContext.ScanAsync<Category>(null);
                List<Category> page = await search.GetNextSetAsync();
                context.Logger.LogLine("Returned: "+page.Count);
                var response = new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.OK,
                    Body = JsonConvert.SerializeObject(page),
                    Headers = responseHeaders
                };
                
                return response;
            }
            catch (Exception ex)
            {
                context.Logger.LogLine(ex.ToString());
                var response = new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    Body = JsonConvert.SerializeObject(new Error("Internal server error")),
                    Headers = responseHeaders
                };
                return response;
            }
        }
        
        /// <summary>
        /// Get all sales status from database
        /// </summary>
        /// <returns>List<SalesStatus></returns>
        public async Task<APIGatewayProxyResponse> GetSalesStatusAsync(ILambdaContext context)
        {
            try
            {
                context.Logger.LogLine("GetSalesStatusAsync");
                var search = this.DDBContext.ScanAsync<SalesStatus>(null);
                List<SalesStatus> page = await search.GetNextSetAsync();
                context.Logger.LogLine("Returned: "+page.Count);
                var response = new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.OK,
                    Body = JsonConvert.SerializeObject(page),
                    Headers = responseHeaders
                };
                
                return response;
            }
            catch (Exception ex)
            {
                context.Logger.LogLine(ex.ToString());
                var response = new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    Body = JsonConvert.SerializeObject(new Error("Internal server error")),
                    Headers = responseHeaders
                };
                return response;
            }
        }
        
        /// <summary>
        /// Get all sale points from database
        /// </summary>
        /// <returns>List<SalePoint></returns>
        public async Task<APIGatewayProxyResponse> GetSalePointsAsync(ILambdaContext context)
        {
            try
            {
                context.Logger.LogLine("GetSalePointsAsync");
                var search = this.DDBContext.ScanAsync<SalePoint>(null);
                List<SalePoint> page = await search.GetNextSetAsync();
                context.Logger.LogLine("Returned: "+page.Count);
                var response = new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.OK,
                    Body = JsonConvert.SerializeObject(page),
                    Headers = responseHeaders
                };
                
                return response;
            }
            catch (Exception ex)
            {
                context.Logger.LogLine(ex.ToString());
                var response = new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    Body = JsonConvert.SerializeObject(new Error("Internal server error")),
                    Headers = responseHeaders
                };
                return response;
            }
        }

        /// <summary>
        /// Generate options header
        /// </summary>
        /// <param name="request"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task<APIGatewayProxyResponse> Options(APIGatewayProxyRequest request, ILambdaContext context)
        {
            context.Logger.LogLine("Options request");

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Headers = new Dictionary<string, string>
                {
                    { "Access-Control-Allow-Origin", "*" },  
                    { "Access-Control-Allow-Methods", "GET, OPTIONS, POST" }, 
                    {"Access-Control-Allow-Headers", "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token"}
                }
            };
                
            return response;
        }
    }
}