﻿namespace LambdaModel.DynamoDB
{
    public class SaleLine
    {
        public int? lineNumber { get; set; }
        public int status{ get; set; }
        public int? promotionId { get; set; }
        public SalesPack? pack { get; set; }
        public string productExternalId { get; set; }
        public string productExternalName { get; set; }
        public int categoryId { get; set; }
        public int? units { get; set; }
        public decimal? appliedVat { get; set; }
        public decimal? priceNoVat { get; set; }
        public decimal? priceVat { get; set; }
        public decimal? totalPriceVat { get; set; }
        public decimal? totalPriceNoVat { get; set; }
        public decimal? totalPricePromotionNoVat { get; set; }
        public decimal? totalPricePromotionVat { get; set; }
        public string? creationDate { get; set; }
        public string modificationDate { get; set; }
        public string cancellationDate { get; set; }
        public string barCode { get; set; }
    }
}