namespace LambdaPost.DynamoDB
{
    public class Contractor
    {
        public int contractorid { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }
}