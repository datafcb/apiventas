﻿namespace LambdaModel.DynamoDB
{
    public class Promotion
    {
        public int promotionId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int promotionType { get; set; }
        public int value { get; set; }
    }
}