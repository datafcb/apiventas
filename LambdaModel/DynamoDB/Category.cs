﻿namespace LambdaModel.DynamoDB
{
    public class Category
    {
        public int categoryId { get; set; }
        public string name { get; set; }
        public int parentId { get; set; }
    }
}