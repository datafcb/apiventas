﻿namespace LambdaModel.DynamoDB
{
    public class UserInfo
    {
        public string name { get; set; }
        public string mail { get; set; }
        public string BID { get; set; }
        public string sociId { get; set; }
    }
}