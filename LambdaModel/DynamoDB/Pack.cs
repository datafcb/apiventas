﻿namespace LambdaModel.DynamoDB
{
    public class Pack
    {
        public int packId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }
}