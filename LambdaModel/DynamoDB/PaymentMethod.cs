﻿namespace LambdaModel.DynamoDB
{
    public class PaymentMethod
    {
        public int paymentMethodId { get; set; }
        public string name { get; set; }
    }
}