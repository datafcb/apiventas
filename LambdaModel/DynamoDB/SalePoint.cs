﻿namespace LambdaModel.DynamoDB
{
    public class SalePoint
    {
        public int salePointId { get; set; }
        public string name { get; set; }
    }
}