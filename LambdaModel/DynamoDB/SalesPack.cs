﻿using System.Collections.Generic;

namespace LambdaModel.DynamoDB
{
    public class SalesPack
    {
        public int packId { get; set; }
        public List<PackProduct> packProducts { get; set; }
    }
}