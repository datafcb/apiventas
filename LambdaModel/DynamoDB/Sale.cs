﻿using System.Collections.Generic;

namespace LambdaModel.DynamoDB
{
    public class Sale
    {
        public string transactionId { get; set; }
        public int paymentMethodId { get; set; }
        public int? isSoci { get; set; }
        public int? isEmployee { get; set; }
        public int? isClickCollect { get; set; }
        public int? isDeliverySeient { get; set; }
        public UserInfo? userInfo { get; set; }
        public decimal? totalPriceVat { get; set; }
        public decimal? totalPriceNoVat { get; set; }
        public List<SaleLine> lines { get; set; }
        public int salePoint { get; set; }
        public int? creationDate { get; set; }
        public int? modificationDate { get; set; }
        public int? cancellationDate { get; set; }
        public bool Exported { get; set; }
        public int ContractorID { get; set; }
    }
}