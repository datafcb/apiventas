﻿namespace LambdaModel.DynamoDB
{
    public class PackProduct
    {
        public string productExternalName { get; set; }
        public string productExternalId { get; set; }
        public int categoryId { get; set; }
    }
}