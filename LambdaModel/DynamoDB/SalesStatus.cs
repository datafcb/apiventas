﻿namespace LambdaModel.DynamoDB
{
    public class SalesStatus
    {
        public int statusId { get; set; }
        public string name { get; set; }
    }
}