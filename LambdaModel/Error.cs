﻿namespace LambdaModel
{
    public class Error
    {
        public Error(string message)
        {
            this.message = message;
        }

        public string message {get; set; }
    }
}