﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LambdaModel.oracle
{
    public partial class VenLinium
    {
        public VenLinium()
        {
            VenLiniaFoodBeverages = new HashSet<VenLiniaFoodBeverage>();
            VenLiniaPackFoodBevs = new HashSet<VenLiniaPackFoodBev>();
        }

        public decimal VltId { get; set; }
        public decimal VltVcuId { get; set; }
        public decimal VltVesId { get; set; }
        public decimal? VltVpmId { get; set; }
        public decimal VltLinia { get; set; }
        public decimal? VltVpoId { get; set; }
        public string VltProducteOrigen { get; set; }
        public string VltProducteOrigenId { get; set; }
        public decimal? VltVpaId { get; set; }
        public decimal? VltVttId { get; set; }
        public string VltLocalitzador { get; set; }
        public string VltFactura { get; set; }
        public string VltNumDocument { get; set; }
        public DateTime VltDataVenda { get; set; }
        public DateTime? VltDataModificacio { get; set; }
        public DateTime? VltDataCancelacio { get; set; }
        public DateTime VltDataCarrega { get; set; }
        public decimal? VltIvaAplicat { get; set; }
        public decimal? VltPreuUnitariSenseIva { get; set; }
        public decimal? VltPreuUnitariIva { get; set; }
        public decimal? VltPreuPromocioSenseIva { get; set; }
        public decimal? VltPreuPromocioIva { get; set; }
        public decimal VltUnitats { get; set; }
        public decimal VltTotalSenseIva { get; set; }
        public decimal VltTotalIva { get; set; }

        public virtual VenCapcalera VltVcu { get; set; }
        public virtual VenEstat VltVes { get; set; }
        public virtual VenPaquet VltVpa { get; set; }
        public virtual VenPromocio VltVpm { get; set; }
        public virtual ICollection<VenLiniaFoodBeverage> VenLiniaFoodBeverages { get; set; }
        public virtual ICollection<VenLiniaPackFoodBev> VenLiniaPackFoodBevs { get; set; }
    }
}
