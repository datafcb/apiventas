﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LambdaModel.oracle
{
    public partial class VenCapcalera
    {
        public VenCapcalera()
        {
            VenLinia = new HashSet<VenLinium>();
        }

        public decimal VcuId { get; set; }
        public string VcuVendaOrigenId { get; set; }
        public string VcuTemporada { get; set; }
        public decimal VcuVorId { get; set; }
        public decimal? VcuVcvId { get; set; }
        public decimal VcuVmpId { get; set; }
        public decimal? VcuVcoId { get; set; }
        public DateTime VcuDataVenda { get; set; }
        public DateTime? VcuDataModificacio { get; set; }
        public DateTime? VcuDataCancelacio { get; set; }
        public DateTime VcuDataCarrega { get; set; }
        public string VcuB2cB2b { get; set; }
        public string VcuPaisIp { get; set; }
        public string VcuPaisTargeta { get; set; }
        public bool? VcuVendaSoci { get; set; }
        public bool? VcuVendaEmpleat { get; set; }
        public bool? VcuEsClickCollect { get; set; }
        public bool? VcuEsDeliverySeient { get; set; }
        public decimal VcuTotalPriceVat { get; set; }
        public decimal VcuTotalPriceNoVat { get; set; }
        public virtual VenComprador VcuVco { get; set; }
        public virtual VenMetodePagament VcuVmp { get; set; }
        public virtual VenOrigen VcuVor { get; set; }
        public virtual ICollection<VenLinium> VenLinia { get; set; }
    }
}
