﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LambdaModel.oracle
{
    public partial class VenNegociFoodBev
    {
        public VenNegociFoodBev()
        {
            VenLiniaFoodBeverages = new HashSet<VenLiniaFoodBeverage>();
        }

        public decimal VnfId { get; set; }
        public string VnfNom { get; set; }

        public virtual ICollection<VenLiniaFoodBeverage> VenLiniaFoodBeverages { get; set; }
    }
}
