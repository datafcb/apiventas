﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LambdaModel.oracle
{
    public partial class VenOrigen
    {
        public VenOrigen()
        {
            VenCapcaleras = new HashSet<VenCapcalera>();
        }

        public decimal VorId { get; set; }
        public string VorNom { get; set; }

        public virtual ICollection<VenCapcalera> VenCapcaleras { get; set; }
    }
}
