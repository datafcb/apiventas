﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LambdaModel.oracle
{
    public partial class VenEstat
    {
        public VenEstat()
        {
            VenLinia = new HashSet<VenLinium>();
        }

        public decimal VesId { get; set; }
        public string VesNom { get; set; }

        public virtual ICollection<VenLinium> VenLinia { get; set; }
    }
}
