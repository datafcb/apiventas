﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LambdaModel.oracle
{
    public partial class VenLiniaFoodBeverage
    {
        public decimal VlfId { get; set; }
        public decimal VlfVltId { get; set; }
        public decimal VlfVnfId { get; set; }
        public decimal VlfVpcId { get; set; }
        public string VlfBarCodeNavision { get; set; }

        public virtual VenLinium VlfVlt { get; set; }
        public virtual VenNegociFoodBev VlfVnf { get; set; }
        public virtual VenProducteCategorium VlfVpc { get; set; }
    }
}
