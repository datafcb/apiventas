﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LambdaModel.oracle
{
    public partial class VenProducteCategorium
    {
        public VenProducteCategorium()
        {
            InverseVpcVpc = new HashSet<VenProducteCategorium>();
            VenLiniaFoodBeverages = new HashSet<VenLiniaFoodBeverage>();
            VenLiniaPackFoodBevs = new HashSet<VenLiniaPackFoodBev>();
        }

        public decimal VpcId { get; set; }
        public decimal? VpcVpcId { get; set; }
        public string VpcNom { get; set; }
        public string VpcTipus { get; set; }

        public virtual VenProducteCategorium VpcVpc { get; set; }
        public virtual ICollection<VenProducteCategorium> InverseVpcVpc { get; set; }
        public virtual ICollection<VenLiniaFoodBeverage> VenLiniaFoodBeverages { get; set; }
        public virtual ICollection<VenLiniaPackFoodBev> VenLiniaPackFoodBevs { get; set; }
    }
}
