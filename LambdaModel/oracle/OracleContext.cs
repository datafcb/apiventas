﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace LambdaModel.oracle
{
    public partial class OracleContext : DbContext
    {
        public OracleContext()
        {
        }

        public OracleContext(DbContextOptions<OracleContext> options)
            : base(options)
        {
        }

        public virtual DbSet<VenCapcalera> VenCapcaleras { get; set; }
        public virtual DbSet<VenComprador> VenCompradors { get; set; }
        public virtual DbSet<VenEstat> VenEstats { get; set; }
        public virtual DbSet<VenLiniaFoodBeverage> VenLiniaFoodBeverages { get; set; }
        public virtual DbSet<VenLiniaPackFoodBev> VenLiniaPackFoodBevs { get; set; }
        public virtual DbSet<VenLinium> VenLinia { get; set; }
        public virtual DbSet<VenMetodePagament> VenMetodePagaments { get; set; }
        public virtual DbSet<VenNegociFoodBev> VenNegociFoodBevs { get; set; }
        public virtual DbSet<VenOrigen> VenOrigens { get; set; }
        public virtual DbSet<VenPaquet> VenPaquets { get; set; }
        public virtual DbSet<VenProducteCategorium> VenProducteCategoria { get; set; }
        public virtual DbSet<VenPromocio> VenPromocios { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string connectionString = Environment.GetEnvironmentVariable("ConnectionString");

            if (string.IsNullOrEmpty(connectionString))
            {
                connectionString =
                    "User Id=AWS_VENTAS;Password=d5iOx0CTmT;Data Source=(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = fcbvorcdbcor-scan.fcbarcelona.net)(PORT = 1471))(CONNECT_DATA =(SERVICE_NAME = dwhpro)));";
            }
            
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseOracle(connectionString, options => options.UseOracleSQLCompatibility("11"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("AWS_VENTAS");

            modelBuilder.Entity<VenCapcalera>(entity =>
            {
                entity.HasKey(e => e.VcuId);

                entity.ToTable("VEN_CAPCALERA", "FCBDWHOWN");

                entity.HasIndex(e => e.VcuVcoId, "FKI_VCO_CAPCALERA");

                entity.HasIndex(e => e.VcuVcvId, "FK_VCV_CAPCALERA");

                entity.HasIndex(e => e.VcuVmpId, "FK_VMP_CAPCALERA");

                entity.HasIndex(e => e.VcuVorId, "FK_VOR_CAPCALERA");

                entity.Property(e => e.VcuId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VCU_ID");

                entity.Property(e => e.VcuB2cB2b)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("VCU_B2C_B2B")
                    .HasDefaultValueSql("'C' \n");

                entity.Property(e => e.VcuDataCancelacio)
                    .HasColumnType("DATE")
                    .HasColumnName("VCU_DATA_CANCELACIO");

                entity.Property(e => e.VcuDataCarrega)
                    .HasColumnType("DATE")
                    .HasColumnName("VCU_DATA_CARREGA");

                entity.Property(e => e.VcuDataModificacio)
                    .HasColumnType("DATE")
                    .HasColumnName("VCU_DATA_MODIFICACIO");

                entity.Property(e => e.VcuDataVenda)
                    .HasColumnType("DATE")
                    .HasColumnName("VCU_DATA_VENDA");

                entity.Property(e => e.VcuPaisIp)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("VCU_PAIS_IP");

                entity.Property(e => e.VcuPaisTargeta)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("VCU_PAIS_TARGETA");

                entity.Property(e => e.VcuTemporada)
                    .IsRequired()
                    .HasMaxLength(9)
                    .IsUnicode(false)
                    .HasColumnName("VCU_TEMPORADA");

                entity.Property(e => e.VcuTotalPriceNoVat)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VCU_TOTAL_PRICE_NO_VAT");

                entity.Property(e => e.VcuTotalPriceVat)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VCU_TOTAL_PRICE_VAT");

                entity.Property(e => e.VcuVcoId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VCU_VCO_ID");

                entity.Property(e => e.VcuVcvId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VCU_VCV_ID");

                entity.Property(e => e.VcuVendaEmpleat)
                    .HasPrecision(1)
                    .HasColumnName("VCU_VENDA_EMPLEAT")
                    .HasDefaultValueSql("0 \n");

                entity.Property(e => e.VcuEsClickCollect)
                    .HasPrecision(1)
                    .HasColumnName("VCU_ES_CLICK_COLLECT")
                    .HasDefaultValueSql("0 \n");

                entity.Property(e => e.VcuEsDeliverySeient)
                    .HasPrecision(1)
                    .HasColumnName("VCU_ES_DELIVERY_SEIENT")
                    .HasDefaultValueSql("0 \n");

                entity.Property(e => e.VcuVendaOrigenId)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("VCU_VENDA_ORIGEN_ID");

                entity.Property(e => e.VcuVendaSoci)
                    .HasPrecision(1)
                    .HasColumnName("VCU_VENDA_SOCI")
                    .HasDefaultValueSql("0 \n");

                entity.Property(e => e.VcuVmpId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VCU_VMP_ID");

                entity.Property(e => e.VcuVorId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VCU_VOR_ID");

                entity.HasOne(d => d.VcuVco)
                    .WithMany(p => p.VenCapcaleras)
                    .HasForeignKey(d => d.VcuVcoId)
                    .HasConstraintName("FK_VCO_CAPCALERA");

                entity.HasOne(d => d.VcuVmp)
                    .WithMany(p => p.VenCapcaleras)
                    .HasForeignKey(d => d.VcuVmpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VMP_CAPCALERA");

                entity.HasOne(d => d.VcuVor)
                    .WithMany(p => p.VenCapcaleras)
                    .HasForeignKey(d => d.VcuVorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VOR_CAPCALERA");
            });

            modelBuilder.Entity<VenComprador>(entity =>
            {
                entity.HasKey(e => e.VcoId)
                    .HasName("VEN_COMPRADOR_PK");

                entity.ToTable("VEN_COMPRADOR", "FCBDWHOWN");

                entity.Property(e => e.VcoId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VCO_ID");

                entity.Property(e => e.VcoBid)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("VCO_BID");

                entity.Property(e => e.VcoIdSoci)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("VCO_ID_SOCI");

                entity.Property(e => e.VcoIdioma)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("VCO_IDIOMA");

                entity.Property(e => e.VcoMail)
                    .HasMaxLength(512)
                    .IsUnicode(false)
                    .HasColumnName("VCO_MAIL");

                entity.Property(e => e.VcoNom)
                    .IsRequired()
                    .HasMaxLength(512)
                    .HasColumnName("VCO_NOM");

                entity.Property(e => e.VcoPais)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("VCO_PAIS");
            });

            modelBuilder.Entity<VenEstat>(entity =>
            {
                entity.HasKey(e => e.VesId)
                    .HasName("VEN_ESTATS_PK");

                entity.ToTable("VEN_ESTATS", "FCBDWHOWN");

                entity.Property(e => e.VesId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VES_ID");

                entity.Property(e => e.VesNom)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasColumnName("VES_NOM");
            });

            modelBuilder.Entity<VenLiniaFoodBeverage>(entity =>
            {
                entity.HasKey(e => e.VlfId)
                    .HasName("VEN_LINIA_FODD_BEVERAGE_PK");

                entity.ToTable("VEN_LINIA_FOOD_BEVERAGE", "FCBDWHOWN");

                entity.HasIndex(e => e.VlfVltId, "FKI_VLT_LINIA_FOOD_BEV");

                entity.HasIndex(e => e.VlfVnfId, "FKI_VNF_LINIA_FOOD_BEV");

                entity.Property(e => e.VlfId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLF_ID");

                entity.Property(e => e.VlfBarCodeNavision)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("VLF_BAR_CODE_NAVISION");

                entity.Property(e => e.VlfVltId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLF_VLT_ID");

                entity.Property(e => e.VlfVnfId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLF_VNF_ID");

                entity.Property(e => e.VlfVpcId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLF_VPC_ID")
                    .HasDefaultValueSql("0 ");

                entity.HasOne(d => d.VlfVlt)
                    .WithMany(p => p.VenLiniaFoodBeverages)
                    .HasForeignKey(d => d.VlfVltId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VLT_LINIA_FOOD_BEV");

                entity.HasOne(d => d.VlfVnf)
                    .WithMany(p => p.VenLiniaFoodBeverages)
                    .HasForeignKey(d => d.VlfVnfId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VNF_LINIA_FOOD_BEV");

                entity.HasOne(d => d.VlfVpc)
                    .WithMany(p => p.VenLiniaFoodBeverages)
                    .HasForeignKey(d => d.VlfVpcId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VPC_LINEA_FOOD_BEVERAGE");
            });

            modelBuilder.Entity<VenLiniaPackFoodBev>(entity =>
            {
                entity.HasKey(e => e.VlaId)
                    .HasName("VEN_LINIA_PACK_FOOD_BEV_PK");

                entity.ToTable("VEN_LINIA_PACK_FOOD_BEV", "FCBDWHOWN");

                entity.Property(e => e.VlaId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLA_ID");

                entity.Property(e => e.VlaProducteOrigen)
                    .IsRequired()
                    .HasMaxLength(512)
                    .IsUnicode(false)
                    .HasColumnName("VLA_PRODUCTE_ORIGEN");

                entity.Property(e => e.VlaProducteOrigenId)
                    .IsRequired()
                    .HasMaxLength(128)
                    .IsUnicode(false)
                    .HasColumnName("VLA_PRODUCTE_ORIGEN_ID");

                entity.Property(e => e.VlaVltId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLA_VLT_ID");

                entity.Property(e => e.VlaVpcId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLA_VPC_ID")
                    .HasDefaultValueSql("0 ");

                entity.HasOne(d => d.VlaVlt)
                    .WithMany(p => p.VenLiniaPackFoodBevs)
                    .HasForeignKey(d => d.VlaVltId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VLA_LINIA");

                entity.HasOne(d => d.VlaVpc)
                    .WithMany(p => p.VenLiniaPackFoodBevs)
                    .HasForeignKey(d => d.VlaVpcId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VLA_CATEGORIA");
            });

            modelBuilder.Entity<VenLinium>(entity =>
            {
                entity.HasKey(e => e.VltId)
                    .HasName("VEN_LINIA_PK");

                entity.ToTable("VEN_LINIA", "FCBDWHOWN");

                entity.HasIndex(e => e.VltVcuId, "FKI_VCU_LINIA");

                entity.HasIndex(e => e.VltVesId, "FKI_VES_LINIA");

                entity.HasIndex(e => e.VltVpaId, "FKI_VPA_LINIA");

                entity.HasIndex(e => e.VltVpmId, "FKI_VPM_LINIA");

                entity.HasIndex(e => e.VltVpoId, "FKI_VPO_LINIA");

                entity.HasIndex(e => e.VltVttId, "FKI_VTT_LINIA");

                entity.Property(e => e.VltId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLT_ID");

                entity.Property(e => e.VltDataCancelacio)
                    .HasColumnType("DATE")
                    .HasColumnName("VLT_DATA_CANCELACIO");

                entity.Property(e => e.VltDataCarrega)
                    .HasColumnType("DATE")
                    .HasColumnName("VLT_DATA_CARREGA");

                entity.Property(e => e.VltDataModificacio)
                    .HasColumnType("DATE")
                    .HasColumnName("VLT_DATA_MODIFICACIO");

                entity.Property(e => e.VltDataVenda)
                    .HasColumnType("DATE")
                    .HasColumnName("VLT_DATA_VENDA");

                entity.Property(e => e.VltFactura)
                    .HasMaxLength(512)
                    .IsUnicode(false)
                    .HasColumnName("VLT_FACTURA");

                entity.Property(e => e.VltIvaAplicat)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLT_IVA_APLICAT");

                entity.Property(e => e.VltLinia)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLT_LINIA");

                entity.Property(e => e.VltLocalitzador)
                    .HasMaxLength(512)
                    .IsUnicode(false)
                    .HasColumnName("VLT_LOCALITZADOR");

                entity.Property(e => e.VltNumDocument)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("VLT_NUM_DOCUMENT");

                entity.Property(e => e.VltPreuPromocioIva)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLT_PREU_PROMOCIO_IVA");

                entity.Property(e => e.VltPreuPromocioSenseIva)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLT_PREU_PROMOCIO_SENSE_IVA");

                entity.Property(e => e.VltPreuUnitariIva)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLT_PREU_UNITARI_IVA");

                entity.Property(e => e.VltPreuUnitariSenseIva)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLT_PREU_UNITARI_SENSE_IVA");

                entity.Property(e => e.VltProducteOrigen)
                    .HasMaxLength(512)
                    .IsUnicode(false)
                    .HasColumnName("VLT_PRODUCTE_ORIGEN");

                entity.Property(e => e.VltProducteOrigenId)
                    .HasMaxLength(128)
                    .IsUnicode(false)
                    .HasColumnName("VLT_PRODUCTE_ORIGEN_ID");

                entity.Property(e => e.VltTotalIva)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLT_TOTAL_IVA");

                entity.Property(e => e.VltTotalSenseIva)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLT_TOTAL_SENSE_IVA");

                entity.Property(e => e.VltUnitats)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLT_UNITATS");

                entity.Property(e => e.VltVcuId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLT_VCU_ID");

                entity.Property(e => e.VltVesId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLT_VES_ID");

                entity.Property(e => e.VltVpaId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLT_VPA_ID");

                entity.Property(e => e.VltVpmId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLT_VPM_ID");

                entity.Property(e => e.VltVpoId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLT_VPO_ID");

                entity.Property(e => e.VltVttId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VLT_VTT_ID");

                entity.HasOne(d => d.VltVcu)
                    .WithMany(p => p.VenLinia)
                    .HasForeignKey(d => d.VltVcuId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VCU_LINIA");

                entity.HasOne(d => d.VltVes)
                    .WithMany(p => p.VenLinia)
                    .HasForeignKey(d => d.VltVesId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VES_LINIA");

                entity.HasOne(d => d.VltVpa)
                    .WithMany(p => p.VenLinia)
                    .HasForeignKey(d => d.VltVpaId)
                    .HasConstraintName("FK_VPA_LINIA");

                entity.HasOne(d => d.VltVpm)
                    .WithMany(p => p.VenLinia)
                    .HasForeignKey(d => d.VltVpmId)
                    .HasConstraintName("FK_VPM_LINIA");
            });

            modelBuilder.Entity<VenMetodePagament>(entity =>
            {
                entity.HasKey(e => e.VmpId)
                    .HasName("VEN_METODE_PAGAMENT_PK");

                entity.ToTable("VEN_METODE_PAGAMENT", "FCBDWHOWN");

                entity.Property(e => e.VmpId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VMP_ID");

                entity.Property(e => e.VmpNom)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasColumnName("VMP_NOM");
            });

            modelBuilder.Entity<VenNegociFoodBev>(entity =>
            {
                entity.HasKey(e => e.VnfId)
                    .HasName("VEN_NEGOCI_FOOD_BEV_PK");

                entity.ToTable("VEN_NEGOCI_FOOD_BEV", "FCBDWHOWN");

                entity.Property(e => e.VnfId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VNF_ID");

                entity.Property(e => e.VnfNom)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("VNF_NOM");
            });

            modelBuilder.Entity<VenOrigen>(entity =>
            {
                entity.HasKey(e => e.VorId)
                    .HasName("VEN_ORIGEN_PK");

                entity.ToTable("VEN_ORIGEN", "FCBDWHOWN");

                entity.Property(e => e.VorId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VOR_ID");

                entity.Property(e => e.VorNom)
                    .IsRequired()
                    .HasMaxLength(128)
                    .IsUnicode(false)
                    .HasColumnName("VOR_NOM");
            });

            modelBuilder.Entity<VenPaquet>(entity =>
            {
                entity.HasKey(e => e.VpaId)
                    .HasName("VEN_PAQUET_PK");

                entity.ToTable("VEN_PAQUET", "FCBDWHOWN");

                entity.Property(e => e.VpaId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VPA_ID");

                entity.Property(e => e.VpaDescripcio)
                    .IsRequired()
                    .HasMaxLength(512)
                    .IsUnicode(false)
                    .HasColumnName("VPA_DESCRIPCIO");

                entity.Property(e => e.VpaNom)
                    .IsRequired()
                    .HasMaxLength(512)
                    .IsUnicode(false)
                    .HasColumnName("VPA_NOM");
            });

            modelBuilder.Entity<VenProducteCategorium>(entity =>
            {
                entity.HasKey(e => e.VpcId)
                    .HasName("VEN_PRODUCTE_CATEGORIA_PK");

                entity.ToTable("VEN_PRODUCTE_CATEGORIA", "FCBDWHOWN");

                entity.Property(e => e.VpcId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VPC_ID");

                entity.Property(e => e.VpcNom)
                    .IsRequired()
                    .HasMaxLength(512)
                    .IsUnicode(false)
                    .HasColumnName("VPC_NOM");

                entity.Property(e => e.VpcTipus)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("VPC_TIPUS");

                entity.Property(e => e.VpcVpcId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VPC_VPC_ID");

                entity.HasOne(d => d.VpcVpc)
                    .WithMany(p => p.InverseVpcVpc)
                    .HasForeignKey(d => d.VpcVpcId)
                    .HasConstraintName("FK_VPC_CATEGORIA");
            });

            modelBuilder.Entity<VenPromocio>(entity =>
            {
                entity.HasKey(e => e.VpmId)
                    .HasName("VEN_PROMOCIO_PK");

                entity.ToTable("VEN_PROMOCIO", "FCBDWHOWN");

                entity.Property(e => e.VpmId)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VPM_ID");

                entity.Property(e => e.VpmDescripcio)
                    .IsRequired()
                    .HasMaxLength(256)
                    .IsUnicode(false)
                    .HasColumnName("VPM_DESCRIPCIO");

                entity.Property(e => e.VpmNom)
                    .IsRequired()
                    .HasMaxLength(256)
                    .IsUnicode(false)
                    .HasColumnName("VPM_NOM");

                entity.Property(e => e.VpmType)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VPM_TYPE");

                entity.Property(e => e.VpmValor)
                    .HasColumnType("NUMBER")
                    .HasColumnName("VPM_VALOR");
            });

            modelBuilder.HasSequence("SEQ_NEGOCI_FOOD_BEV", "FCBDWHOWN");

            modelBuilder.HasSequence("SEQ_VEN_CAPCALERA", "FCBDWHOWN");

            modelBuilder.HasSequence("SEQ_VEN_COMPRADOR", "FCBDWHOWN");

            modelBuilder.HasSequence("SEQ_VEN_ESTATS", "FCBDWHOWN");

            modelBuilder.HasSequence("SEQ_VEN_LINIA", "FCBDWHOWN");

            modelBuilder.HasSequence("SEQ_VEN_LINIA_FOOD_BEVERAGE", "FCBDWHOWN");

            modelBuilder.HasSequence("SEQ_VEN_LINIA_PACK_FOOD_BEV", "FCBDWHOWN");

            modelBuilder.HasSequence("SEQ_VEN_METODE_PAGAMENT", "FCBDWHOWN");

            modelBuilder.HasSequence("SEQ_VEN_ORIGEN", "FCBDWHOWN");

            modelBuilder.HasSequence("SEQ_VEN_PAQUET", "FCBDWHOWN");

            modelBuilder.HasSequence("SEQ_VEN_PRODUCTE_CATEGORIA", "FCBDWHOWN");

            modelBuilder.HasSequence("SEQ_VEN_PROMOCIO", "FCBDWHOWN");

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
