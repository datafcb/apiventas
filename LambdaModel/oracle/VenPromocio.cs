﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LambdaModel.oracle
{
    public partial class VenPromocio
    {
        public VenPromocio()
        {
            VenLinia = new HashSet<VenLinium>();
        }

        public decimal VpmId { get; set; }
        public string VpmNom { get; set; }
        public string VpmDescripcio { get; set; }
        public decimal VpmType { get; set; }
        public decimal VpmValor { get; set; }

        public virtual ICollection<VenLinium> VenLinia { get; set; }
    }
}
