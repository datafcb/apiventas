﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LambdaModel.oracle
{
    public partial class VenPaquet
    {
        public VenPaquet()
        {
            VenLinia = new HashSet<VenLinium>();
        }

        public decimal VpaId { get; set; }
        public string VpaNom { get; set; }
        public string VpaDescripcio { get; set; }

        public virtual ICollection<VenLinium> VenLinia { get; set; }
    }
}
