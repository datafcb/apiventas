﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LambdaModel.oracle
{
    public partial class VenComprador
    {
        public VenComprador()
        {
            VenCapcaleras = new HashSet<VenCapcalera>();
        }

        public decimal VcoId { get; set; }
        public string VcoNom { get; set; }
        public string VcoMail { get; set; }
        public string VcoPais { get; set; }
        public string VcoIdioma { get; set; }
        public string VcoBid { get; set; }
        public string VcoIdSoci { get; set; }

        public virtual ICollection<VenCapcalera> VenCapcaleras { get; set; }
    }
}
