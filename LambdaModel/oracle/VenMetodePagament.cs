﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LambdaModel.oracle
{
    public partial class VenMetodePagament
    {
        public VenMetodePagament()
        {
            VenCapcaleras = new HashSet<VenCapcalera>();
        }

        public decimal VmpId { get; set; }
        public string VmpNom { get; set; }

        public virtual ICollection<VenCapcalera> VenCapcaleras { get; set; }
    }
}
