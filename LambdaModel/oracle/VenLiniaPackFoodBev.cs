﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LambdaModel.oracle
{
    public partial class VenLiniaPackFoodBev
    {
        public decimal VlaId { get; set; }
        public decimal VlaVpcId { get; set; }
        public decimal VlaVltId { get; set; }
        public string VlaProducteOrigen { get; set; }
        public string VlaProducteOrigenId { get; set; }

        public virtual VenLinium VlaVlt { get; set; }
        public virtual VenProducteCategorium VlaVpc { get; set; }
    }
}
