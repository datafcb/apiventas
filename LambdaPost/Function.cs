using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using LambdaModel;
using LambdaModel.DynamoDB;
using LambdaModel.oracle;
using LambdaPost.DynamoDB;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace LambdaPost
{
    public class Functions
    {
        IDynamoDBContext DDBContext { get; set; }
        private OracleContext _context;

        private Dictionary<string, string> responseHeaders = new Dictionary<string, string>
            {{"Content-Type", "application/json"}, {"Access-Control-Allow-Origin", "*"}};

        public Functions()
        {
            AWSConfigsDynamoDB.Context.TypeMappings[typeof(Sale)] =
                new Amazon.Util.TypeMapping(typeof(Sale), "ApiVentas_sales");
            AWSConfigsDynamoDB.Context.TypeMappings[typeof(PaymentMethod)] =
                new Amazon.Util.TypeMapping(typeof(PaymentMethod), "ApiVentas_paymentMethod");
            AWSConfigsDynamoDB.Context.TypeMappings[typeof(Pack)] =
                new Amazon.Util.TypeMapping(typeof(Pack), "ApiVentas_pack");
            AWSConfigsDynamoDB.Context.TypeMappings[typeof(Promotion)] =
                new Amazon.Util.TypeMapping(typeof(Promotion), "ApiVentas_promotion");
            AWSConfigsDynamoDB.Context.TypeMappings[typeof(Category)] =
                new Amazon.Util.TypeMapping(typeof(Category), "ApiVentas_category");
            AWSConfigsDynamoDB.Context.TypeMappings[typeof(SalesStatus)] =
                new Amazon.Util.TypeMapping(typeof(SalesStatus), "ApiVentas_salesStatus");
            AWSConfigsDynamoDB.Context.TypeMappings[typeof(SalePoint)] =
                new Amazon.Util.TypeMapping(typeof(SalePoint), "ApiVentas_salePoint");
            AWSConfigsDynamoDB.Context.TypeMappings[typeof(Contractor)] =
                new Amazon.Util.TypeMapping(typeof(Contractor), "ApiVentas_contractor");
            var config = new DynamoDBContextConfig {Conversion = DynamoDBEntryConversion.V2};
            DDBContext = new DynamoDBContext(new AmazonDynamoDBClient(), config);
        }

        /// <summary>
        /// Add one sale to database
        /// </summary>
        /// <param name="request"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task<APIGatewayProxyResponse> AddSalesAsync(APIGatewayProxyRequest request, ILambdaContext context)
        {
            try
            {
                context.Logger.LogLine("AddSalesAsync");

                var body = request.Body;
                if (request.IsBase64Encoded)
                {
                    var base64EncodedBytes = System.Convert.FromBase64String(request.Body);
                    body = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                }

                context.Logger.LogLine(body);

                var cognitoGroups = request.RequestContext.Authorizer.Claims["cognito:groups"];

                var sale = JsonConvert.DeserializeObject<Sale>(body);
                sale.Exported = false;

                var contractorId = await GetContractorID(context, cognitoGroups);
                context.Logger.LogLine("ContractorID: " + contractorId);
                sale.ContractorID = contractorId;

                var response = await CheckSaleHeader(sale);

                if (response != null)
                {
                    context.Logger.LogLine($"Response code: {response.StatusCode}");
                    context.Logger.LogLine($"Response message: {response.Body}");
                    return response;
                }

                foreach (var line in sale.lines)
                {
                    response = await CheckSaleLine(line);

                    if (response != null)
                    {
                        context.Logger.LogLine($"Response code: {response.StatusCode}");
                        context.Logger.LogLine($"Response message: {response.Body}");
                        return response;
                    }
                }

                await DDBContext.SaveAsync<Sale>(sale);
                response = new APIGatewayProxyResponse
                {
                    StatusCode = (int) HttpStatusCode.OK,
                    Body = JsonConvert.SerializeObject(new Error(sale.transactionId)),
                    Headers = responseHeaders
                };
                context.Logger.LogLine($"Response code: {response.StatusCode}");
                context.Logger.LogLine($"Response message: {response.Body}");
                return response;
            }
            catch (Exception ex)
            {
                context.Logger.LogLine(ex.ToString());
                var response = new APIGatewayProxyResponse
                {
                    StatusCode = (int) HttpStatusCode.InternalServerError,
                    Body = JsonConvert.SerializeObject(new Error("Internal server error")),
                    Headers = responseHeaders
                };
                context.Logger.LogLine($"Response code: {response.StatusCode}");
                context.Logger.LogLine($"Response message: {response.Body}");
                return response;
            }
        }

        /// <summary>
        /// Syncronize pending sales with oracle db
        /// </summary>
        /// <param name="context"></param>
        public async Task SyncPendingSales(ILambdaContext context)
        {
            try
            {
                _context = new OracleContext();
                context.Logger.LogLine("SyncPendingSales");
                
                string connectionString = Environment.GetEnvironmentVariable("ConnectionString");
                context.Logger.LogLine(connectionString);
                context.Logger.LogLine(_context.Database.GetConnectionString());

                List<ScanCondition> conditions = new List<ScanCondition>();
                conditions.Add(new ScanCondition("Exported", ScanOperator.Equal, false));
                var response = await DDBContext.ScanAsync<Sale>(conditions).GetRemainingAsync();
                context.Logger.LogLine($"Pending {response.Count} sales to sync");
                for (int i = 0; i < response.Count; i++)
                {
                    var sale = response[i];
                    context.Logger.LogLine($"Initializing sale {i + 1} / {response.Count}");
                    try
                    {
                        var header =
                            _context.VenCapcaleras.FirstOrDefault(a => a.VcuVendaOrigenId == sale.transactionId);
                        if (header == null)
                        {
                            header = new VenCapcalera();
                            header.VcuId = GetNext("SEQ_VEN_CAPCALERA");
                            _context.VenCapcaleras.Add(header);
                        }

                        var creationDate = UnixTimeStampToDateTime(Convert.ToDouble(sale.creationDate));

                        header.VcuVendaOrigenId = sale.transactionId;
                        header.VcuTemporada = GetSeasson(creationDate);
                        header.VcuVorId = sale.ContractorID;
                        header.VcuVmpId = sale.paymentMethodId;
                        header.VcuDataVenda = creationDate;
                        if (sale.modificationDate != null)
                        {
                            header.VcuDataModificacio =
                                UnixTimeStampToDateTime(Convert.ToDouble(sale.modificationDate));
                        }

                        if (sale.cancellationDate != null)
                        {
                            header.VcuDataCancelacio = UnixTimeStampToDateTime(Convert.ToDouble(sale.cancellationDate));
                        }

                        header.VcuDataCarrega = DateTime.Now;
                        header.VcuB2cB2b = "C";
                        header.VcuVendaSoci = sale.isSoci == 1;
                        header.VcuVendaEmpleat = sale.isEmployee == 1;
                        header.VcuEsClickCollect = sale.isClickCollect == 1;
                        header.VcuEsDeliverySeient = sale.isDeliverySeient == 1;
                        header.VcuTotalPriceVat = sale.totalPriceVat ?? 0;
                        header.VcuTotalPriceNoVat = sale.totalPriceNoVat ?? 0;

                        if (sale.userInfo != null)
                        {
                            var userInfo = _context.VenCompradors.FirstOrDefault(a => a.VcoBid == sale.userInfo.BID);

                            if (userInfo == null)
                            {
                                userInfo = _context.VenCompradors.FirstOrDefault(a =>
                                    a.VcoIdSoci == sale.userInfo.sociId);
                            }

                            if (userInfo == null)
                            {
                                userInfo = new VenComprador();
                                userInfo.VcoId = GetNext("SEQ_VEN_COMPRADOR");
                                _context.VenCompradors.Add(userInfo);
                            }

                            userInfo.VcoNom = sale.userInfo.name;
                            userInfo.VcoMail = sale.userInfo.mail;
                            userInfo.VcoBid = sale.userInfo.BID;
                            userInfo.VcoIdSoci = sale.userInfo.sociId;
                            header.VcuVcoId = userInfo.VcoId;
                        }

                        for (int j = 0; j < sale.lines.Count; j++)
                        {
                            context.Logger.LogLine($"Initializing line {j + 1} / {sale.lines.Count}");
                            var saleLine = sale.lines[j];

                            var line = _context.VenLinia.FirstOrDefault(a =>
                                a.VltVcuId == header.VcuId && a.VltLinia == saleLine.lineNumber);
                            if (line == null)
                            {
                                line = new VenLinium();
                                line.VltId = GetNext("SEQ_VEN_LINIA");
                                _context.VenLinia.Add(line);
                            }

                            line.VltVcuId = header.VcuId;
                            line.VltLinia = saleLine.lineNumber ?? 1;
                            line.VltVesId = saleLine.status;
                            if (saleLine.promotionId != null)
                            {
                                line.VltVpmId = saleLine.promotionId;
                            }

                            line.VltProducteOrigenId = saleLine.productExternalId;
                            line.VltProducteOrigen = saleLine.productExternalName;

                            line.VltDataVenda = UnixTimeStampToDateTime(Convert.ToDouble(saleLine.creationDate));
                            if (saleLine.modificationDate != null)
                            {
                                line.VltDataModificacio =
                                    UnixTimeStampToDateTime(Convert.ToDouble(saleLine.modificationDate));
                            }

                            if (saleLine.cancellationDate != null)
                            {
                                line.VltDataCancelacio =
                                    UnixTimeStampToDateTime(Convert.ToDouble(saleLine.cancellationDate));
                            }

                            line.VltDataCarrega = DateTime.Now;
                            line.VltIvaAplicat = saleLine.appliedVat;
                            line.VltPreuUnitariSenseIva = saleLine.priceNoVat;
                            line.VltPreuUnitariIva = saleLine.priceVat;
                            line.VltUnitats = saleLine.units ?? 0;
                            line.VltTotalIva = saleLine.totalPriceVat ?? 0;
                            line.VltTotalSenseIva = saleLine.totalPriceNoVat ?? 0;
                            line.VltPreuPromocioIva = saleLine.totalPricePromotionVat ?? 0;
                            line.VltPreuPromocioSenseIva = saleLine.totalPricePromotionNoVat ?? 0;

                            if (saleLine.pack != null)
                            {
                                line.VltVpaId = saleLine.pack.packId;
                                for (int k = 0; k < saleLine.pack.packProducts.Count; k++)
                                {
                                    var product = saleLine.pack.packProducts[k];
                                    var linePackFoodBeverage = _context.VenLiniaPackFoodBevs.FirstOrDefault(a =>
                                        a.VlaVltId == line.VltId && a.VlaProducteOrigenId == product.productExternalId);
                                    if (linePackFoodBeverage == null)
                                    {
                                        linePackFoodBeverage = new VenLiniaPackFoodBev();
                                        linePackFoodBeverage.VlaId = GetNext("SEQ_VEN_LINIA_PACK_FOOD_BEV");
                                        linePackFoodBeverage.VlaVltId = line.VltId;
                                        _context.VenLiniaPackFoodBevs.Add(linePackFoodBeverage);
                                    }

                                    linePackFoodBeverage.VlaProducteOrigen = product.productExternalName;
                                    linePackFoodBeverage.VlaProducteOrigenId = product.productExternalId;
                                    linePackFoodBeverage.VlaVpcId = product.categoryId;
                                }
                            }

                            var lineFoodBeverage =
                                _context.VenLiniaFoodBeverages.FirstOrDefault(a => a.VlfVltId == line.VltId);
                            if (lineFoodBeverage == null)
                            {
                                lineFoodBeverage = new VenLiniaFoodBeverage();
                                lineFoodBeverage.VlfId = GetNext("SEQ_VEN_LINIA_FOOD_BEVERAGE");

                                _context.VenLiniaFoodBeverages.Add(lineFoodBeverage);
                            }

                            lineFoodBeverage.VlfVltId = line.VltId;
                            lineFoodBeverage.VlfVpcId = saleLine.categoryId;
                            lineFoodBeverage.VlfVnfId = 1;
                            lineFoodBeverage.VlfBarCodeNavision = saleLine.barCode;
                        }

                        _context.SaveChanges();

                        sale.Exported = true;
                        await DDBContext.SaveAsync(sale);
                        context.Logger.LogLine($"Sale {sale.transactionId} marked as exported ");
                    }
                    catch (Exception e)
                    {
                        context.Logger.LogLine(e.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                context.Logger.LogLine(ex.ToString());
            }
        }

        /// <summary>
        /// Convert timestamp to DateTime in GMT+2 time zone
        /// </summary>
        /// <param name="unixTimeStamp"></param>
        /// <returns></returns>
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp);
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("Europe/Madrid");
            DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(dtDateTime, cstZone);
            return cstTime;
        }

        /// <summary>
        /// Get contractor ID from AWS Cognito group
        /// </summary>
        /// <param name="context"></param>
        /// <param name="groupName"></param>
        /// <returns></returns>
        private async Task<int> GetContractorID(ILambdaContext context, string groupName)
        {
            var groups = groupName.Split(",");
            int i = 0;

            while (i < groups.Length)
            {
                Contractor contractor = await DDBContext.LoadAsync<Contractor>(groups[i]);
                if (contractor != null)
                {
                    context.Logger.LogLine("Contractor: " + contractor.contractorid + " " + contractor.name);
                    return contractor.contractorid;
                }

                ++i;
            }

            return -1;
        }

        /// <summary>
        /// Validate Sale Header fields
        /// </summary>
        /// <param name="sale"></param>
        /// <returns></returns>
        private async Task<APIGatewayProxyResponse> CheckSaleHeader(Sale sale)
        {
            if (string.IsNullOrEmpty(sale.transactionId))
            {
                var errorResponse = new APIGatewayProxyResponse
                {
                    StatusCode = 520,
                    Body = JsonConvert.SerializeObject(new Error("transactionId is empty")),
                    Headers = responseHeaders
                };

                return errorResponse;
            }

            var paymentMethod = await DDBContext.LoadAsync<PaymentMethod>(sale.paymentMethodId);
            if (paymentMethod == null)
            {
                var errorResponse = new APIGatewayProxyResponse
                {
                    StatusCode = 501,
                    Body = JsonConvert.SerializeObject(new Error("Payment method not found")),
                    Headers = responseHeaders
                };

                return errorResponse;
            }

            var salePoint = await DDBContext.LoadAsync<SalePoint>(sale.salePoint);
            if (salePoint == null)
            {
                var errorResponse = new APIGatewayProxyResponse
                {
                    StatusCode = 502,
                    Body = JsonConvert.SerializeObject(new Error("Sale point not found")),
                    Headers = responseHeaders
                };

                return errorResponse;
            }

            if (sale.totalPriceVat == null)
            {
                var errorResponse = new APIGatewayProxyResponse
                {
                    StatusCode = 503,
                    Body = JsonConvert.SerializeObject(new Error("Total price vat is empty")),
                    Headers = responseHeaders
                };

                return errorResponse;
            }

            if (sale.totalPriceNoVat == null)
            {
                var errorResponse = new APIGatewayProxyResponse
                {
                    StatusCode = 504,
                    Body = JsonConvert.SerializeObject(new Error("Total price no vat is empty")),
                    Headers = responseHeaders
                };

                return errorResponse;
            }

            if (sale.lines == null || sale.lines.Count == 0)
            {
                var errorResponse = new APIGatewayProxyResponse
                {
                    StatusCode = 505,
                    Body = JsonConvert.SerializeObject(new Error("Sale with empty lines")),
                    Headers = responseHeaders
                };

                return errorResponse;
            }

            if (sale.creationDate == null)
            {
                var errorResponse = new APIGatewayProxyResponse
                {
                    StatusCode = 506,
                    Body = JsonConvert.SerializeObject(new Error("Creation date incorrect")),
                    Headers = responseHeaders
                };

                return errorResponse;
            }

            return null;
        }

        /// <summary>
        /// Validate Sale Line fields
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        private async Task<APIGatewayProxyResponse> CheckSaleLine(SaleLine line)
        {
            if (line.lineNumber == null)
            {
                var errorResponse = new APIGatewayProxyResponse
                {
                    StatusCode = 507,
                    Body = JsonConvert.SerializeObject(new Error("No line number")),
                    Headers = responseHeaders
                };

                return errorResponse;
            }

            var status = await DDBContext.LoadAsync<SalesStatus>(line.status);
            if (status == null)
            {
                var errorResponse = new APIGatewayProxyResponse
                {
                    StatusCode = 508,
                    Body = JsonConvert.SerializeObject(new Error($"Line {line.lineNumber}, status not found")),
                    Headers = responseHeaders
                };

                return errorResponse;
            }

            if (line.promotionId > 0)
            {
                var promotion = await DDBContext.LoadAsync<Promotion>(line.promotionId);
                if (promotion == null)
                {
                    var errorResponse = new APIGatewayProxyResponse
                    {
                        StatusCode = 509,
                        Body = JsonConvert.SerializeObject(new Error($"Line {line.lineNumber}, promotion not found")),
                        Headers = responseHeaders
                    };

                    return errorResponse;
                }
            }

            var category = await DDBContext.LoadAsync<Category>(line.categoryId);
            if ((category == null || (category.parentId == 0 && category.categoryId > 0)) && line.pack == null)
            {
                var errorResponse = new APIGatewayProxyResponse
                {
                    StatusCode = 510,
                    Body = JsonConvert.SerializeObject(new Error($"Line {line.lineNumber}, category not found")),
                    Headers = responseHeaders
                };

                return errorResponse;
            }

            if (line.pack != null)
            {
                var pack = await DDBContext.LoadAsync<Pack>(line.pack.packId);
                if (pack == null)
                {
                    var errorResponse = new APIGatewayProxyResponse
                    {
                        StatusCode = 511,
                        Body = JsonConvert.SerializeObject(new Error($"Line {line.lineNumber}, pack not found")),
                        Headers = responseHeaders
                    };

                    return errorResponse;
                }

                if (line.pack.packProducts == null || line.pack.packProducts.Count == 0)
                {
                    var errorResponse = new APIGatewayProxyResponse
                    {
                        StatusCode = 512,
                        Body = JsonConvert.SerializeObject(new Error($"Line {line.lineNumber}, pack without products")),
                        Headers = responseHeaders
                    };

                    return errorResponse;
                }

                foreach (var product in line.pack.packProducts)
                {
                    var packProductCategory = await DDBContext.LoadAsync<Category>(product.categoryId);
                    if (packProductCategory == null ||
                        (packProductCategory.parentId == 0 && packProductCategory.categoryId > 0))
                    {
                        var errorResponse = new APIGatewayProxyResponse
                        {
                            StatusCode = 513,
                            Body = JsonConvert.SerializeObject(new Error(
                                $"Line {line.lineNumber} and {product.productExternalId}, category not found")),
                            Headers = responseHeaders
                        };

                        return errorResponse;
                    }
                }

                if (line.units == null)
                {
                    var errorResponse = new APIGatewayProxyResponse
                    {
                        StatusCode = 514,
                        Body = JsonConvert.SerializeObject(new Error($"Line {line.lineNumber}, units is empty")),
                        Headers = responseHeaders
                    };

                    return errorResponse;
                }

                if (line.appliedVat == null)
                {
                    var errorResponse = new APIGatewayProxyResponse
                    {
                        StatusCode = 515,
                        Body = JsonConvert.SerializeObject(new Error($"Line {line.lineNumber}, appliedVat is empty")),
                        Headers = responseHeaders
                    };

                    return errorResponse;
                }

                if (line.priceNoVat == null)
                {
                    var errorResponse = new APIGatewayProxyResponse
                    {
                        StatusCode = 516,
                        Body = JsonConvert.SerializeObject(new Error($"Line {line.lineNumber}, priceNoVat is empty")),
                        Headers = responseHeaders
                    };

                    return errorResponse;
                }

                if (line.priceVat == null)
                {
                    var errorResponse = new APIGatewayProxyResponse
                    {
                        StatusCode = 517,
                        Body = JsonConvert.SerializeObject(new Error($"Line {line.lineNumber}, priceVat is empty")),
                        Headers = responseHeaders
                    };

                    return errorResponse;
                }

                if (line.totalPriceVat == null)
                {
                    var errorResponse = new APIGatewayProxyResponse
                    {
                        StatusCode = 518,
                        Body =
                            JsonConvert.SerializeObject(new Error($"Line {line.lineNumber}, totalPriceVat is empty")),
                        Headers = responseHeaders
                    };

                    return errorResponse;
                }

                if (line.creationDate == null)
                {
                    var errorResponse = new APIGatewayProxyResponse
                    {
                        StatusCode = 519,
                        Body = JsonConvert.SerializeObject(
                            new Error($"Line {line.lineNumber}, Creation date incorrect")),
                        Headers = responseHeaders
                    };

                    return errorResponse;
                }
            }

            return null;
        }

        /// <summary>
        /// Calculate the Seassion of the sale
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private string GetSeasson(DateTime date)
        {
            if (date.Month <= 6)
            {
                return $"{date.Year - 1}-{date.Year}";
            }
            else
            {
                return $"{date.Year}-{date.Year + 1}";
            }
        }

        /// <summary>
        /// Get next ID value
        /// </summary>
        /// <param name="sequenceName"></param>
        /// <returns></returns>
        private int GetNext(string sequenceName)
        {
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = $"SELECT FCBDWHOWN.{sequenceName}.NEXTVAL FROM DUAL";
                _context.Database.OpenConnection();
                using (var reader = command.ExecuteReader())
                {
                    reader.Read();
                    var result = reader.GetInt32(0);
                    return result;
                }
            }
        }
    }
}